
#include <QPainter>
#include <QKeyEvent>

#include <math.h>

#include "mandelbrotwidget.h"


//! [0]
const double DefaultCenterX = -0.637011f;
const double DefaultCenterY = -0.0395159f;
const double DefaultScale = 0.00403897f;

const double ZoomInFactor = 0.8f;
const double ZoomOutFactor = 1 / ZoomInFactor;
const int ScrollStep = 20;
//! [0]

//! [1]
MandelbrotWidget::MandelbrotWidget(QWidget *parent)
    : QWidget(parent)
{
    centerX = DefaultCenterX;
    centerY = DefaultCenterY;
    pixmapScale = DefaultScale;
    curScale = DefaultScale;

    connect(&thread, SIGNAL(renderedImage(QImage,double)), this, SLOT(updatePixmap(QImage,double)));

    setWindowTitle(tr("Mandelbrot"));
#ifndef QT_NO_CURSOR
    setCursor(Qt::CrossCursor);
#endif
    resize(550, 400);

}
//! [1]

//! [2]
void MandelbrotWidget::paintEvent(QPaintEvent * /* event */)
{
    QPainter painter(this);
    painter.fillRect(rect(), Qt::black);

    if (pixmap.isNull()) {
        painter.setPen(Qt::white);
        painter.drawText(rect(), Qt::AlignCenter, tr("Rendering initial image, please wait..."));
//! [2] //! [3]
        return;
//! [3] //! [4]
    }
//! [4]

//! [5]
    if (curScale == pixmapScale) {
//! [5] //! [6]
        painter.drawPixmap(pixmapOffset, pixmap);
//! [6] //! [7]
    } else {
//! [7] //! [8]
        double scaleFactor = pixmapScale / curScale;
        int newWidth = int(pixmap.width() * scaleFactor);
        int newHeight = int(pixmap.height() * scaleFactor);
        int newX = pixmapOffset.x() + (pixmap.width() - newWidth) / 2;
        int newY = pixmapOffset.y() + (pixmap.height() - newHeight) / 2;

        painter.save();
        painter.translate(newX, newY);
        painter.scale(scaleFactor, scaleFactor);
        QRectF exposed = painter.matrix().inverted().mapRect(rect()).adjusted(-1, -1, 1, 1);
        painter.drawPixmap(exposed, pixmap, exposed);
        painter.restore();
    }
//! [8] //! [9]

    QString text = tr("Use mouse wheel or the '+' and '-' keys to zoom. "
                      "Press and hold left mouse button to scroll.");
    QFontMetrics metrics = painter.fontMetrics();
    int textWidth = metrics.width(text);

    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(0, 0, 0, 127));
    painter.drawRect((width() - textWidth) / 2 - 5, 0, textWidth + 10, metrics.lineSpacing() + 5);
    painter.setPen(Qt::white);
    painter.drawText((width() - textWidth) / 2, metrics.leading() + metrics.ascent(), text);
}
//! [9]

//! [10]
void MandelbrotWidget::resizeEvent(QResizeEvent * /* event */)
{
    //computation is separate thread
    //thread.render(centerX, centerY, curScale, size());
    //computation is main thread
    //render(centerX, centerY, curScale, size());
}
//! [10]

//! [11]
void MandelbrotWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Plus:
        zoom(ZoomInFactor);
        break;
    case Qt::Key_Minus:
        zoom(ZoomOutFactor);
        break;
    case Qt::Key_Left:
        scroll(-ScrollStep, 0);
        break;
    case Qt::Key_Right:
        scroll(+ScrollStep, 0);
        break;
    case Qt::Key_Down:
        scroll(0, -ScrollStep);
        break;
    case Qt::Key_Up:
        scroll(0, +ScrollStep);
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}
//! [11]

#ifndef QT_NO_WHEELEVENT
//! [12]
void MandelbrotWidget::wheelEvent(QWheelEvent *event)
{
    int numDegrees = event->delta() / 8;
    double numSteps = numDegrees / 15.0f;
    zoom(pow(ZoomInFactor, numSteps));
}
//! [12]
#endif

//! [13]
void MandelbrotWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        lastDragPos = event->pos();
}
//! [13]

//! [14]
void MandelbrotWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        pixmapOffset += event->pos() - lastDragPos;
        lastDragPos = event->pos();
        update();
    }
}
//! [14]

//! [15]
void MandelbrotWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        pixmapOffset += event->pos() - lastDragPos;
        lastDragPos = QPoint();

        int deltaX = (width() - pixmap.width()) / 2 - pixmapOffset.x();
        int deltaY = (height() - pixmap.height()) / 2 - pixmapOffset.y();
        scroll(deltaX, deltaY);
    }
}
//! [15]

//! [16]
void MandelbrotWidget::updatePixmap(const QImage &image, double scaleFactor)
{
    if (!lastDragPos.isNull())
        return;

    pixmap = QPixmap::fromImage(image);
    pixmapOffset = QPoint();
    lastDragPos = QPoint();
    pixmapScale = scaleFactor;
    update();
}
//! [16]

//! [17]
void MandelbrotWidget::zoom(double zoomFactor)
{
    curScale *= zoomFactor;
    update();
    //computation is separate thread
    //thread.render(centerX, centerY, curScale, size());
    //computation is main thread
    render(centerX, centerY, curScale, size());
}
//! [17]

//! [18]
void MandelbrotWidget::scroll(int deltaX, int deltaY)
{
    centerX += deltaX * curScale;
    centerY += deltaY * curScale;
    update();
    //computation is separate thread
    //thread.render(centerX, centerY, curScale, size());
    //computation is main thread
    //render(centerX, centerY, curScale, size());
}
//! [18]
//!
void MandelbrotWidget::render(double centerX, double centerY, double scaleFactor, QSize resultSize)
{
  enum { ColormapSize = 512 };
  uint colormap[ColormapSize];
  for (int i = 0; i < ColormapSize; ++i)
      colormap[i] = thread.rgbFromWaveLength(380.0 + (i * 400.0 / ColormapSize));

  int halfWidth = resultSize.width() / 2;
//! [4] //! [5]
  int halfHeight = resultSize.height() / 2;
  QImage image(resultSize, QImage::Format_RGB32);

  const int NumPasses = 8;
  int pass = 0;
  while (pass < NumPasses) {
      const int MaxIterations = (1 << (2 * pass + 6)) + 32;
      const int Limit = 4;
      bool allBlack = true;

      for (int y = -halfHeight; y < halfHeight; ++y) {
          uint *scanLine =
                  reinterpret_cast<uint *>(image.scanLine(y + halfHeight));
          double ay = centerY + (y * scaleFactor);

          for (int x = -halfWidth; x < halfWidth; ++x) {
              double ax = centerX + (x * scaleFactor);
              double a1 = ax;
              double b1 = ay;
              int numIterations = 0;

              do {
                  ++numIterations;
                  double a2 = (a1 * a1) - (b1 * b1) + ax;
                  double b2 = (2 * a1 * b1) + ay;
                  if ((a2 * a2) + (b2 * b2) > Limit)
                      break;

                  ++numIterations;
                  a1 = (a2 * a2) - (b2 * b2) + ax;
                  b1 = (2 * a2 * b2) + ay;
                  if ((a1 * a1) + (b1 * b1) > Limit)
                      break;
              } while (numIterations < MaxIterations);

              if (numIterations < MaxIterations) {
                  *scanLine++ = colormap[numIterations % ColormapSize];
                  allBlack = false;
              } else {
                  *scanLine++ = qRgb(0, 0, 0);
              }
          }
      }

      if (allBlack && pass == 0) {
          pass = 4;
      } else {
              updatePixmap(image, scaleFactor);
//! [5] //! [6]
          ++pass;
      }
//! [6] //! [7]
  }
}

